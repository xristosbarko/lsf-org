#################
Paid Contributors
#################

Paid contributors are a sub-set of our Core Contributors.
In this page you can find information this is specific for our Paid
contributors, otherwise known as employees.

******
Hiring
******

Besides the onboarding specified in the Core Contributors documentation there a
few extra information that is needed to be communicated and arranged, contracts
to be signed etc.
The hiring responsible would know how to open an issue tracking this.

************
Compensation
************

Your monthly compensation will be transfered to you at the last week of each
month or within a week of you filling your invoice at bank @ libre.space (if
you are under a freelance contract).

********
Benefits
********

Besides your paid compensation in LSF, as a regular paid contributor you are
entitled to the following benefits:

#. A work laptop (see :doc:`../projects/procurement`)
#. An unlimeted data and voice cell phone plan

********
Time off
********

When you need to be off work please make sure to follow the following steps:

#. Coordinate with your peers on all projects you are part of, notifying them
   for your intention to leave.
#. Make sure that no blockers are created by your leave.
#. Assign a person (or more if needed) to be responsible for urgent needs at
   your absence.
#. Open `a Time Off issue
   <https://gitlab.com/librespacefoundation/lsf-hr-leaves/-/issues/new?issuable_template=Staff%20-%20Time%20Off&issue[title]=Unavailability%20-%20Full%20Name>`_.
#. Make sure you update your calendar to reflect your time off.
#. Remember to update the `issue board
   <https://gitlab.com/librespacefoundation/lsf-hr-leaves/-/boards/5337628>`_
   to reflect the status of your absence by moving it to the correct column
   once in effect or after it's completed.
#. Enjoy your time off! (hopefully not a sick leave!)


Paid time-off
=============

In case of planned time off (vacations etc) give ample notice to your
colleagues and ensure no blockers are created by your absence.
Try to coorindate as much in advance as possible.
For minimum there needs to be a full working day in advance to give everyone a
chance to respond and coordinate.

Sick leave
==========

In case of emergency or any unforseen event, try to follow the steps to the
extend possible.

Unpaid time off
===============

If you need to take a longer break, consider the option of unpaid time-off.
Reach out to Human Resources team to coordinate the details which will be
varying depending on your employment status.

Parental leave
==============

Please reach out to Human Resources team to discuss specifics based on your
employment type.

*************
Bank holidays
*************

In addition to the official bank holidays in Greece, the following list of
holidays have unofficially a similar status at LSF:

* Holy Spirit Day - Monday, moveable

It is agreed by all contributors that during these days, no meetings shall be
scheduled and no contributor is expected to be on call.

******
Levels
******

At LSF we implement a job levels system with a common path up to level C3.
After the common path there are two distinct paths that can be followed; the
"Technical Path" and the "Managerial Path".

+--------------------+--------------------+------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     | .. rst-class::     | .. rst-class::                                                               | .. rst-class::               |
|    wy-text-center  |    wy-text-center  |    wy-text-center                                                            |    wy-text-center            |
|                    |                    |                                                                              |                              |
| Code               | Path               | Description                                                                  | Typical Years of Experience  |
+====================+====================+==============================================================================+==============================+
| .. rst-class::     | .. rst-class::     | You are probably an intern and you are developing your technical abilities   | .. rst-class::               |
|    wy-text-center  |    wy-text-center  | while still studying.                                                        |    wy-text-center            |
|                    |                    |                                                                              |                              |
| C0                 | Common             |                                                                              | 0                            |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | You are an entry level contributor you are developing your technical         | .. rst-class::               |
|    wy-text-center  |                    | abilities.                                                                   |    wy-text-center            |
|                    |                    | This is probably your first or second job.                                   |                              |
| C1                 |                    |                                                                              | 0                            |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | At this level you expand upon your foundational knowledge and develop an     | .. rst-class::               |
|    wy-text-center  |                    | understanding of best practices in your field.                               |    wy-text-center            |
|                    |                    | You can work independently and regularly assist levels C0-C1 with tasks or   |                              |
| C2                 |                    | troubleshooting problems.                                                    | 3                            |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | As a career contributor you have an established knowlege and experience in   | .. rst-class::               |
|    wy-text-center  |                    | your field.                                                                  |    wy-text-center            |
|                    |                    | You are working mostly independently and are able to take initiative and     |                              |
| C3                 |                    | lead small teams and small projects.                                         | 5                            |
+--------------------+--------------------+------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     | .. rst-class::     | As a staff engineer you need to have a comprehensive knowledge of            | .. rst-class::               |
|    wy-text-center  |    wy-text-center  | engineering development practices.                                           |    wy-text-center            |
|                    |                    | Engineering at this level includes performing complex tasks and              |                              |
| T1                 | Technical          | implementing complete systems.                                               | 6                            |
|                    |                    | The job is often highly collaborative and may include leading teams or       |                              |
|                    |                    | mentoring more junior engineers.                                             |                              |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | As a more experienced staff engineer you need to have a comprehensive        | .. rst-class::               |
|    wy-text-center  |                    | knowledge of engineering development practices.                              |    wy-text-center            |
|                    |                    | You are coming up with compelete system designs and implementations of them. |                              |
| T2                 |                    | You are building up your expertise in the field in a distinctive way.        | 7                            |
|                    |                    | The job is often highly collaborative and does include leading teams and     |                              |
|                    |                    | mentoring more junior engineers.                                             |                              |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | At this level you have the ability to perform tasks and large engineering    | .. rst-class::               |
|    wy-text-center  |                    | projects with Independence and expertise.                                    |    wy-text-center            |
|                    |                    | You are often responsible for guiding or mentoring other contributors, and   |                              |
| T3                 |                    | regularly lead technical projects for external partners and clients.         | 10                           |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | This is the highest level on the technical engineering career track.         | .. rst-class::               |
|    wy-text-center  |                    | You are top in your field.                                                   |    wy-text-center            |
|                    |                    | You make strategic decisions about which team members should work on which   |                              |
| T4                 |                    | areas of a project and you provide technical and professional leadership     | 15+                          |
|                    |                    | for other engineers.                                                         |                              |
+--------------------+--------------------+------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     | .. rst-class::     | As an engineering manager you regularly lead teams or projects, and you      | .. rst-class::               |
|    wy-text-center  |     wy-text-center | provide support for initiatives throughout many areas within the             |    wy-text-center            |
|                    |                    | organization.                                                                |                              |
| M1                 | Managerial         |                                                                              | 5                            |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | As a more experienced engineering manager you have a broad understanding of  | .. rst-class::               |
|    wy-text-center  |                    | all aspects relating to the design, innovation and development of products   |    wy-text-center            |
|                    |                    | and solutions.                                                               |                              |
| M2                 |                    | You are able to able to prioritize workloads, tasks and assignments to       | 7                            |
|                    |                    | efficiently manage team projects.                                            |                              |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | As a director you are responsible for managing multiple teams and            | .. rst-class::               |
|    wy-text-center  |                    | coordinating with team leads or engineer managers.                           |    wy-text-center            |
|                    |                    | You have a keen understanding of the broad picture behind the organization   |                              |
| M3                 |                    | initiatives and our development goals.                                       | 10                           |
|                    |                    | You also have an expert level understanding in your field (engineering or    |                              |
|                    |                    | managerial).                                                                 |                              |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | You are a high-level executive who manages teams across the organization.    | .. rst-class::               |
|    wy-text-center  |                    | You are responsible for designing and delivering projects and products that  |    wy-text-center            |
|                    |                    | meet the organization goals.                                                 |                              |
| M4                 |                    | You have a tactical and strategic expertise as well as strong interpersonal  | 15+                          |
|                    |                    | and leadership skills.                                                       |                              |
+--------------------+                    +------------------------------------------------------------------------------+------------------------------+
| .. rst-class::     |                    | You have the highest executive position within the organization, providing   | .. rst-class::               |
|    wy-text-center  |                    | leadership and guidance across all different fields and teams.               |    wy-text-center            |
|                    |                    | You are responsible for implementing the vision and strategy of the          |                              |
| M5                 |                    | organization at the highest level.                                           | 15+                          |
+--------------------+--------------------+------------------------------------------------------------------------------+------------------------------+

*************
Other matters
*************

If you have any other inquiries related to people operations, please don't
hesitate to reach out to us via email at hr @ libre.space. We will be happy
to assist you.
