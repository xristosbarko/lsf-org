# Contributor Onboarding #

## Prerequisites ##

- [ ] GitLab account
- [ ] PGP key ID
- [ ] An onboarding "buddy": <!-- GitLab username of buddy -->

## Actions ##

- [ ] Open issue on [IT operations support](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=LSF%20-%20Add%20contributor&issue[title]=Add%20%3CFull%20Name%3E%20contributor) to request adding yourself as a contributor
- [ ] Ensure IT operations team completes your request before continuing
- [~] Get informed on [how to join a project](https://docs.libre.space/en/stable/projects/FIXME)
- [ ] Get informed on [how to make requests to HR](https://docs.libre.space/en/stable/people/paid-contributors.html#other-matters) or [announcing leaves](https://docs.libre.space/en/stable/people/paid-contributors.html#time-off)
- [ ] Update personal info on your [LSF Cloud](https://cloud.libre.space/settings/user) account, including phone number and location
- [ ] Update your vCard entry on [LSF Cloud](https://cloud.libre.space/apps/contacts/), including phone number, location and PGP key
- [ ] Verify PGP ID with HR team (paid-only)
- [ ] Get informed about [paid contributor benefits](https://docs.libre.space/en/stable/people/paid-contributors.html#benefits) (paid-only)
- [~] Open issue on [Libre Space Communications](https://gitlab.com/librespacefoundation/lsf-comms-servicedesk) for adding you in the contributors list on `About Us` page of [libre.space](https://libre.space) website
- [ ] Overview running LSF projects by visiting the [LSF Core Kanban board](https://gitlab.com/librespacefoundation/lsf-core/-/boards/3342848) and [LSF Documentation](https://docs.libre.space/en/stable/projects/projects-portfolio.html#currently-active-projects)
- [ ] Get informed about [regular LSF meetings](https://docs.libre.space/en/stable/collaboration/meetings.html#regular-lsf-meetings)
- [ ] Receive a welcoming greeting from all LSF members :)
