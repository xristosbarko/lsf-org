###################
LSF Code of Conduct
###################

.. contents::
   :local:
   :backlinks: none


The strength of the Libre Space Foundation is its people; who make up a
majestic, diverse, collaborative and inclusive community.
As such, we at Libre Space Foundation have compiled the Participation
Guidelines for the LSF Community, including the code of conduct and statement
of Principles.

These are inspired by the LSF Manifesto.
And they are created with primarily one thing in mind: creating an environment
where humans from around the world will be able to contribute, learn and evolve
at their own pace.
With mutual respect and freedom to share ideas and opinions that will help make
Space humanity’s future.
With these Guidelines, the LSF Community should be granting its members an
opportunity to explore (their potential freely), develop (their skills), use
(the knowledge acquired), and (help them) thrive differently.
A way to ensure the longevity, sustainability, openness, equality of those
efforts for all humanity ... (and for all the members of the community alike).

********************************
The LSF Guidelines in a nutshell
********************************

*Celebrate our Similarities and Accommodate our Differences*

The LSF Community is made up of individuals coming from many different cultures
and backgrounds.
Cultural differences can encompass everything; from religious beliefs to
political views,  to personal habits and clothing and to technology as well.
Be respectful of people with different cultural practices, attitudes, ideas and
beliefs.
Focus on eliminating your own biases and discriminatory practices.
Instead of criticising, work on eliminating your prejudices.
Always put yourself into the other person’s shoes-think of their needs from
their point of view.
In interaction, do not ignore the preferred titles (including pronouns) and be
careful with using the appropriate tone of voice.
Respect people’s right to privacy and confidentiality.
Respect their process and the time they need to get accustomed to new
practices, information and a new environment.
Be open to learning from others and helping others to learn.
Respect the time they need to adapt and allow for questions, worries and
thoughts while learning.
Interactions with people from different cultures take time to ease into a
smooth process.
Be respectful, open and inclusive.

*******************************
The LSF Statement of Principles
*******************************

*We value discussion, trust, accountability and collaboration*

**Be Open**

To knowledge, approaches, different opinions.
To making mistakes, asking questions, providing answers.
About your problems, obstacles and new ideas.

**Be Collaborative**

With everyone within the community and outside of it (for that matter).
Help educate those who know less than you do, and ask to acquire knowledge.
Steer away from unstructured criticism. Feedback is always welcome but
judgement is not.

**Be Respectful**

Differences of opinion, approaches and techniques will arise, but knowing how
to solve those is of primary significance.
Please be kind and courteous.
There’s no need to be mean or rude.

**Be Considerate**

Be aware of the impact of your words and your behaviour and how these may be
affecting others.
Intense interactions may affect other people in ways you fail to understand.

**Be Transparent**

If you disagree state your disagreement.
If you are facing a problem let your close collaborators know.
Be direct with the things you disagree on and form your argument in a
constructive way.

Create a Safe, respectful and collaborative environment for everyone to thrive
in regardless of their:

* Background
* Family status
* Gender
* Gender identity or expression
* Marital status
* Sex
* Sexual orientation
* Native language
* Age
* Ability
* Race and/or ethnicity
* Caste
* National origin
* Ethnic Origin
* Socioeconomic status
* Religion
* Political Views
* Geographic location
* Any other dimension of diversity

The LSF Principles are created to set the tone for individuals and groups to
interact and collaborate freely and respectfully to the community’s mutual
advantage.

The LSF Code of Conduct illustrates both the expected and prohibited behaviour.

***********************
The LSF Code of Conduct
***********************

Sharing Different Ideas, Dealing with Disagreements, Carrying Inclusive Conversations
=====================================================================================

We value discussion and sharing of different ideas, approaches and points of
view.
Therefore, consciously seeking diverse perspectives.
Ask for different opinions.
Diversity of views and of people on teams fuels innovative solutions, even if
the process is not always comfortable or smooth.
Encourage all voices.
Allow space for new perspectives to be heard and try to listen actively.

Be open to different possibilities, to different truths.
But most importantly be open to the possibility of being wrong.

If you find yourself dominating a discussion, it is significant to step back
and encourage other voices to speak.
Be aware of how much time is taken up by the dominant members of the group.
Provide alternative ways to contribute or participate when possible.

In a conversation,  be inclusive by respecting and facilitating people’s
participation in the interaction.
Everyone should be joining a conversation regardless of  whether they are:
Remote, A native speaker or not, Coming from a different culture, Located in a
different time zone, Facing any number of challenges that make their
participation difficult

Stand up for your opinion and approach and hold your ground if you disagree.
A well-structured conversation will teach us all something new.
While disagreement is welcome poor manners and/or abusive behaviour is not.
And it will not be tolerated.

Be Open and Direct when we disagree and when we believe that improvement is
necessary.
Withholding hard truths from collaborators does not make the process better-in
fact it worsens the situation.
But keep unstructured critique to a minimum.

Be direct, constructive and positive.
Positive is the keyword here.
Step up when you must take responsibility for the impact of your actions, words
and your mistakes – if someone says they have been harmed through your words or
actions, listen carefully, apologize sincerely, and correct the behaviour going
forward.

Providing feedback and Criticism
================================

Being respectful when providing criticism is as important as being open to
receiving it.
Be conscious and on the lookout for the right moment to share the criticism
with the other person.
Be direct with delivering the truth but do so in a kind and respectful way.
Do not be offended if it is hard for others to receive your truth.
Allow time and space for the other person to receive the information you
shared.
Make sure the feedback you share is focused solely on the other person’s work
and not on their personality.
Make it clear that the approach or the technique followed is falling short and
not the other person’s skills or aptitude or their capacity to do something.
Refrain from using diminishing language.

Collaborations, leadership, accountability
==========================================

If you don’t know something, ask questions and seek answers.
Ask for help when unsure.
Nobody is expected to be perfect or to hold all the knowledge and answers
within a community.
Be open to asking and receiving questions always in a respectful way.
Asking questions early on helps avoid many misunderstandings and problems.
When asked a question, be responsive and helpful.

Take up leadership and responsibility.
Lead by example and encourage other members to do so.
Encourage new participants to feel empowered to lead, to share ideas, to take
action,  to experiment when they feel that a project should be handled
differently in order for it to improve.
Leadership can be exercised by anyone simply by taking action.
There is no need to wait for recognition when the opportunity to lead presents
itself.

Match your words with your actions.
Your actions not only do they influence others but they also have an impact on
the way the Community works, communicates and collaborates.
Be a link of inclusivity and openness.
Design your projects and work for inclusion.
To enable others to become a part of it.
Hold yourself and others accountable for inclusive behaviours and make
decisions based on the Principles of the Libre Space Manifesto.

When somebody disengages themselves from a project or decides to leave the
project altogether, it is preferred that they do so in a way that eliminates
(if possible) or minimises disruption of the project.
They should notify people in due time and take the necessary steps to ensure
that others can pick up where they left off.

Unacceptable Behaviours
=======================

The following behaviours are unacceptable within our community:

* Personal Attacks. In a diverse Community, conflicts will inevitably arise.
  But differences in opinion or approach often lead to frustration and to a
  personal attack.
  It is not acceptable to insult, demean or belittle others.
  Attacking someone for their opinions, beliefs and ideas is not acceptable.
* Unwelcome Sexual Attention or Physical Contact.
  Unwelcome sexual attention or unwelcome physical contact is not acceptable.
  This includes sexualized comments, jokes or imagery in interactions,
  communications or presentation materials, as well as inappropriate touching,
  groping, or sexual advances.
* Violence and threats of violence
* Derogatory comments, related to gender, gender identity and expression,
  sexual orientation, disability, mental illness, physical appearance, body
  size, race, religion, politics or socioeconomic status.
  Avoid using overtly sexual aliases or other nicknames that might detract from
  a friendly, safe and welcoming environment for all.
* Posting or threatening to post other people's personally identifying
  information (“doxing”).
* Deliberate intimidation
* Advocating for, or encouraging, any of the above behaviour.
* Private harassment is also unacceptable.

No matter who you are, if you feel you have been or are being harassed or made
uncomfortable by a community member, please contact *conduct at libre.space*
immediately.
Whether you’re a regular contributor, a paid employee, a volunteer or a
newcomer, we care about making this community a safe place for you and we’ve
got your back.
A community where people feel uncomfortable or threatened is not a productive
and inclusive community.

Consequences of Unacceptable Behavior
=====================================

Unacceptable behaviour from any community member (paid employee or volunteer),
including those with decision-making authority, will not be tolerated.

Anyone asked to stop unacceptable behaviour is expected to comply immediately.

We don’t tolerate behaviour that excludes people in socially marginalized
groups.

If a community member engages in unacceptable behaviour, the LSF team may take
any action they deem appropriate, up to and including a temporary ban or
permanent expulsion from the community without warning.

Reports of harassment/discrimination will be promptly and thoroughly
investigated by the people responsible for the safety of the LSF environment.
Appropriate measures will be taken to address the situation.

In addition, any members of the LSF Community who abuse the reporting process
will be considered to be in violation of these guidelines and subject to the
same consequences.  False reporting, especially to retaliate or exclude, will
not be accepted or tolerated.

When and How to Use These Guidelines
====================================

These guidelines outline the behaviour expectations of all the members of the
community and contributors.
Whether offline or online, during virtual events or individual conversations,
online meetings or on the public channels, throughout all the activities,
operations, and projects.
Your collaboration is necessary and your participation is contingent upon
following these guidelines throughout all the LSF activities, including but not
limited to:

* Collaborating in Libre Space Foundation spaces (hackerspace.gr).
* Collaborating with other contributors and other community members online or
  co-located.
* Representing LSF at public events.
* Representing LSF in social media (official accounts, staff accounts, personal
  accounts, Facebook pages).
* Participating in workshops and training whether online or offline.
* Participating in the Libre Space Foundation Community forums, mailing lists,
  wiki pages, websites, chat channels, groups or person-to-person meetings, and
  all LSF-related correspondence and communication.

The Code of Conduct and its Guidelines are an active topic as we continuously
seek to make the community even more inclusive and diverse.
We anticipate and welcome the Community’s feedback in order to help us further
enhance and outline the appropriate boundaries so that we create a safe and
inclusive space (see what we did there..) for everyone to thrive in.

Reporting
=========

If you believe you’re experiencing unacceptable behaviour that will not be
tolerated as outlined above, please contact *conduct @ libre dot space* to
report.
Reports are received by Nikoletta and Fredy.

After receiving a concise description of your situation, we will review and
determine the next steps.

Please also report to us if you observe a potentially dangerous situation,
someone in distress, or violations of these guidelines, even if the situation
is not happening to you.

If you feel you have been unfairly accused of violating these guidelines,
please follow the same reporting process.

Ask Questions
=============

Feel free to reach out to us and ask any questions you might have about these
guidelines.
Your input is welcome.

License and Attribution
=======================

This set of guidelines is distributed under CC-BY-SA

Modifications and Updates to these Guidelines
=============================================

The LSF Participation Guidelines (Statement of Principles and Code of Conduct)
will be amended from time to time so as to include, support and protect the
well being of the Community and its members.
This also means that the procedures may also be updated from time to time.
Therefore, you are kindly requested to keep an eye on the Guidelines and be
aware that your agreement to comply with the guidelines will be deemed
agreement to any changes to it.
