import re, json
from docutils import nodes
from docutils.parsers.rst import Directive
from pathlib import Path
import gitlab
from jinja2 import Environment, FileSystemLoader

BASE_DIR = Path(__file__).resolve().parent.parent

class ProjectTableDirective(Directive):
    required_arguments = 2

    ACTIVE_PROJECTS = "active_projects"
    ADMINISTRATIVE_PROJECTS = "administrative_projects"

    PROJECT_TYPES = {
        ACTIVE_PROJECTS: "active",
        ADMINISTRATIVE_PROJECTS: "administrative",
    }

    def get_projects(self):
        PROJECT = "librespacefoundation/lsf-core"
        ON_GOING = "Ongoing"
        ADMINISTRATIVE = "Administrative"
        TEMPLATE_PATH = "templates"

        if not (BASE_DIR / "data/active_projects.json").exists():
            # active projects
            issues = get_issues(PROJECT,labels=[ON_GOING], exclude_labels=ADMINISTRATIVE)
            create_template_file(issues, f"{BASE_DIR}/{TEMPLATE_PATH}/active-projects")
        if not (BASE_DIR / "data/administrative_projects.json").exists():
            # administrative projects
            issues = get_issues(PROJECT, labels=[ON_GOING, ADMINISTRATIVE])
            create_template_file(issues, f"{BASE_DIR}/{TEMPLATE_PATH}/administrative-projects")

    def run(self):
        self.get_projects()
        project_type = self.PROJECT_TYPES[self.arguments[0]]
        headers = self.arguments[1].split(',')

        # Read the data from the json file to populate the table
        with open(f"{BASE_DIR}/data/{project_type}_projects.json", "r") as f:
            data = json.load(f)

        table_body = [
            [
                f'[[{item["project"]["title"]}]][[{item["project"]["url"]}]]',
                f'[[{item["manager"].get("name", "N/A")}]][[{item["manager"].get("url")}]]',
                f'[[{item["champion"].get("name", "N/A")}]][[{item["champion"].get("url")}]]',
            ]
            for item in data
        ]

        # Table initialization
        table = nodes.table()

        tgroup = nodes.tgroup(cols=len(headers))
        table += tgroup

        for _ in range(len(headers)):
            tgroup += nodes.colspec()

        # Table header
        thead = nodes.thead()
        tgroup += thead

        header_row = nodes.row()
        for header in headers:
            entry = nodes.entry()
            entry += nodes.paragraph(text=header)
            entry['classes'].append('header')
            header_row += entry
        thead += header_row

        # Table Body
        tbody = nodes.tbody()
        tgroup += tbody

        for row_data in table_body:
            row = nodes.row()
            for cell_data in row_data:
                entry = nodes.entry()
                for paragraph_line in cell_data.splitlines():
                    paragraph = self.parse_entry(paragraph_line)
                    entry += paragraph
                row += entry
            tbody += row

        return [table]

    def parse_entry(self, entry_text):
        paragraph = nodes.paragraph()
        pattern = r"\[\[(.*?)\]\]"
        matches = re.findall(pattern, entry_text)

        if matches:
            title, url = matches
            if url != "None":
                link = nodes.reference(refuri=url, text=title)
                paragraph += link
            else:
                paragraph += nodes.Text(title)
        else:
            paragraph += nodes.Text(entry_text)
        return paragraph


def get_issues(project, labels=[], exclude_labels=[]):
    # Create GitLab client
    gl_client = gitlab.Gitlab.from_config()

    # Get project
    project = gl_client.projects.get(project, lazy=True)

    # Excluding issues with specific labels
    extra_params = {
        'not[labels]': ",".join(exclude_labels),
    }

    # Get issues with labels
    try:
        issues = project.issues.list(
            get_all=True,
            labels=",".join(labels),
            confidential=False,
            iterator=False,
            state='opened',
            order_by='created_at',
            sort='asc',
            **extra_params,
        )
    except gitlab.exceptions.GitlabListError:
        return []

    # Create parsed issues list
    items = []
    for issue in issues:
        manager = {}
        champion = {}
        project_url = None

        # project url / public link
        pattern = r"## Public link\n\n(.*)"
        match = re.search(pattern, issue.description)
        if match:
            project_url = match.group(1)

        # users full name and profile urls
        for label in issue.labels:
            if label.startswith("Manager"):
                manager_username = label.split("::@")[-1]
                user = gl_client.users.list(username=manager_username)[0]
                manager = {
                    "name": user.name,
                    "url": user.web_url,
                }
            elif label.startswith("Champion"):
                champion_username = label.split("::@")[-1]
                user = gl_client.users.list(username=champion_username)[0]
                champion = {
                    "name": user.name,
                    "url": user.web_url,
                }

        # Create issue item
        item = {
            'project': {
                'title': issue.title,
                'url': project_url,
            },
            'manager': manager,
            'champion': champion,
        }

        # Append to items list
        items.append(item)

    return items


def create_template_file(items, template):
    template_path = Path(template)

    if template_path.is_dir():
        # List all Jinja2 templates
        template_path_list = list(template_path.glob('**/*.j2'))

        for template_path_item in template_path_list:
            # Create Jinja2 environment
            j2_env = Environment(loader=FileSystemLoader(
                '/' if template_path_item.is_absolute() else ''))
            # Load Jinja2 template
            template = j2_env.get_template(str(template_path_item))

            # Write rendered template
            template.stream(issues=items).dump(
                str(BASE_DIR / template_path_item.relative_to(template_path).with_suffix('')))

    else:
        # Create Jinja2 environment
        j2_env = Environment(loader=FileSystemLoader(
            '/' if template_path.is_absolute() else ''))

        # Load Jinja2 template
        template = j2_env.get_template(str(template_path))

        # Render template
        print(template.render(issues=items))


def setup(app):
    # Register table directive
    app.add_directive('project_table', ProjectTableDirective)
