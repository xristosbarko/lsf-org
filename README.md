# Main Orginizational Repo for Libre Space Foundation #

In this repo you can find issues that LSF uses to track high level non-repo-specific work.
This repository also contains the source code for documentation of Libre Space Foundation.


## Styling guide ##

reStructeredText in this repository shall follow this styling guide for source code consistency.
These rules are **not** checked as part of quality gating with a CI job.


### Sections ###

The succession of headings shall follow the [Python Developer's Guide for documentation](https://devguide.python.org/documentation/markup/#sections):

  * 1st level, `#` with overline
  * 2nd level, `*` with overline
  * 3rd level, `=`
  * 4th level, `-`
  * 5th level, `^`
  * 6th level, `"`


### Lists ###

Numbered lists shall be auto-numbered using the `#.` sign, unless required otherwise.
Unnumbered lists shall only use `*` sign, unless required otherwise.


### Paragraphs ###

All sentences shall end with a punctuation mark and a newline character; only a single sentence can exist on each line.
Lines shall be wrapped to 79 characters (GNU formatting style).


## Building ##

To clean the build directory, run:

```
$ rm -rf _build
```

To build the documentation, assuming all dependencies are already installed on your system, run:

```
$ make html SPHINXOPTS="-W"
```


### Tox ###

To build the documentation in a Python virtual environment, run:

```
$ tox docs
```

To re-build in a re-created environment, run:

```
$ tox -r docs
```


### sphinx-autobuild ###

To automatically build documentation on any change and access it through a local webserver, run:

```
$ docker-compose up --build
```

By default, a server will start listening on http://localhost:8000 , serving the rendered HTML documents.


## Maintenance ##

Package dependencies for building the documentation are in:

  * `requirements.txt` - Dependencies to build the docs
  * `requirements-dev.txt` - Packages used for development
  * `constraints.txt` - Package version constraints
  * `packages.alpine` - Alpine packages required to build docs

To update the dependencies, edit the version ranges in `requirements.txt` and `requirements-dev.txt` files, and then run `contrib/refresh-requirements-docker.sh` to regenerate the `constraints.txt` file.
