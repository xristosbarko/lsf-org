#######
License
#######

Libre Space Foundation documentation is licensed under a `Creative Commons
Attribution-ShareAlike 4.0 International License
<https://creativecommons.org/licenses/by-sa/4.0/>`_ license:
  
.. include:: LICENSE
   :literal:
