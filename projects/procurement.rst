###########
Procurement
###########


For various project and activity needs, contributors of LSF need to procure
various items or services. Here is a brief outline and some guiding principles

************************************
Procurement process for common items
************************************

The term "common items" refers to items that are readily available on the market and for which there are no specific requirements or questions to be answered by the manufacturer.
Their price is provided by the vendor and there is no need to request a quote (in that case see the relevant section below).
A good example of common items are electronic components.

#. Identify the need and think about possible future evolution of this need.
#. Check with Lab (contacting Aris) on possible existing stock
#. Identify the best vendor possible taking into account lower price but also a
   prioritization with regards to vendor location. The priority is as follows:

  #. EU vendor with valid VAT VIES number
  #. Greek vendor with VAT number
  #. Non-EU vendor with local VAT number (avoid if possible!)
  #. Any vendor without a VAT number (avoid if possible!)

#. Request a pro-forma invoice to be issued (unless specific vendor below) to
   LSF Company details.
#. Forward to bank @ libre.space for payment via bank transfer or via a card
   purchase link. We prefer bank transfer over paypal.
#. Once paid, you will receive confirmation from the people behind bank@
#. Once the items arrive, scan and forward the invoice to bills @ libre.space


*******************************************
Procurement process involving quote request
*******************************************

As is the case with many items, the vendor does not disclose the price on their website and a quote is needed.
A quote may also be required when there is the need to ask for specific information about the product.
In this case, the following information exchange with the vendor is performed.

#. LSF sends an RFQ (Request For Quote) to which the manufacturer replies with a quote. Other questions about the product are included in the RFQ.

#. If there is a high degreee of certainty about the purchase of the product, LSF can ask for additional, more specific information (e.g. materials or components list).
#. If LSF is ready to proceed to purchase, a PO (Purchase Order) is sent to the vendor, to which the vendor replies with a pro-forma invoice.
#. Finally, LSF completes the payment and an invoice is received together with the product.

*****************************************
Procurement information for flight models
*****************************************

When procuring components/subsystems for flight models, some extra information shall be requested from the manufacturer.
Since the depth and quantity of the requested information indicate a further commitment into the procurement, they should be requested at a second stage, not together with the initial quotation request.
Internally, those are requested for quality assurance and risk evaluation purposes, while externally they are sometimes requested by the contracting agencies.
The information to be requested are described below.

#. Components List

   This is basically the Bill Of Materials of the subsystem and it is requested in order to enable LSF to investigate the suitability of individual components in case it is needed.

#. Materials List

   This is more generic than the Components List and includes the materials used in the product.
   It can be used to assess whether the product contains materials unsuitable for the operational environment of the project or materials that can hinder the performance of other subsystems.

#. Inspection Process

   This information request is concerned with the quality inspection procedures to which the manufacturer has subjected the product.

#. Qualifications

   This refers to the standards to which the product complies.

You can copy/paste the following text to request the information above:
```
Please provide with your quote or proforma invoice the following detailed information:
a) Detailed components list, b) Materials list, c) Inspection Processes applied, d) Qualifications of items
```


****************
Specific Vendors
****************

The following vendors have been used extensively in the past and should be
preferred since we have a special setup (See notes)

+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| Vendor                                           | Type           | Username                 | Access                     | Notes                                                |
+==================================================+================+==========================+============================+======================================================+
| `Amazon <https://www.amazon.de/>`_               | Everything     | individual accounts      | All core contributors      | Business account, Purchases with policies            |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Mouser <https://www.mouser.com/>`_              | Components     | librespacefoundation     | Pierros, Agis, Ilias       | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Digikey <https://www.digikey.gr/>`_             | Components     | pierros@libre.space      | Pierros                    | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Farnell <https://export.farnell.com/>`_         | Components     | librespace               | Pierros                    | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `TME <https://www.tme.eu/>`_                     | Components     | pierros_librespace       | Pierros                    | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Mini Circuits <https://www.minicircuits.com/>`_ | Components     |                          | Pierros                    | Details for export control added                     |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Discomp <https://www.discomp.cz/>`_             | Networking     |                          | Pierros, Aris              | Business discount and details entered                |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `JLC PCB <https://jlcpcb.com/>`_                 | PCB Production | procurement@libre.space  | Pierros, Agis, Ilias, Aris | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `LCSC <https://www.lcsc.com/>`_                  | Components     | procurement@libre.space  | Pierros, Agis, Ilias, Aris | Business account, Details completed                  |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+
| `Eurocircuits <https://www.eurocircuits.com/>`_  | PCB Production | individual accounts      | Pierros, Dimitris M        | Business account, ask for individual account invites |
+--------------------------------------------------+----------------+--------------------------+----------------------------+------------------------------------------------------+

If you require a purchase from the vendors above, please reach out dircetly to
someone with access.

A special alias (procurment@libre.space) is created for common accounts. People with access to procurement@ : Pierros, Agis, Ilias, Aris, Dimitris M 


***************
Company Details
***************

| Libre Space Foundation
| Ampatiellou 11
| Athens
| 11144
| Greece
| +302130210437
| VAT ID: EL997539194
| GEMI - Business Registration Number: 133102301000
