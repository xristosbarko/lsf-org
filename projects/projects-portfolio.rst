##################
Projects Portfolio
##################

.. contents::
   :local:
   :backlinks: none

.. _Pierros Papadeas: https://gitlab.com/pierros

********
Overview
********

Libre Space Foundation develops and supports a variety of open-source projects
for space.
For each project there is a project manager and a project champion (more on
those :doc:`roles`).

*************************
Currently active projects
*************************

The following major projects are in active development:

.. project_table:: active_projects Project,Manager,Champion

Other projects can be found in lsf-core repository.

***********************
Administrative projects
***********************

The following projects are administrative projects running vertically across
Libre Space Foundation:

.. project_table:: administrative_projects Project,Manager,Champion

**************************
Previously active projects
**************************

The following major projects have previously been in active development:

+---------------+---------------------+------------------+
| Project       | Project Manager     | Project Champion |
+===============+=====================+==================+
| `OpenSatCom`_ | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+
| UPSat_        | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+

.. _OpenSatCom: https://opensatcom.org
.. _UPSat: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared

************
Contributors
************

Libre Space Foundation is an active and welcoming community around open source
space projects.
We welcome all contributors in our projects and repositories.
We recognize those contributors that have been making considerable
contributions in our projects by inviting them in our "LSF Core Contributors"
group.
Learn more about :doc:`../people/core-contributors`.
